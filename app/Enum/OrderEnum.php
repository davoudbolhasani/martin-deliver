<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class OrderEnum extends Enum
{
    const STATUS_PENDING   = 'pending';
    const STATUS_ACCEPTED  = 'accepted';
    const STATUS_RECEIVED  = 'received';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_CANCELED  = 'canceled';

    /**
     * @var array|string[]
     */
    public static array $complexAllowCancelOrderStatus = [
        self::STATUS_PENDING,
        self::STATUS_ACCEPTED,
    ];

    /**
     * @var array|string[]
     */
    public static array $courierAllowUpdateOrderStatus = [
        self::STATUS_PENDING,
        self::STATUS_ACCEPTED,
        self::STATUS_RECEIVED,
    ];

    /**
     * @var array|string[]
     */
    public static array $courierOrderStatus = [
        self::STATUS_ACCEPTED,
        self::STATUS_RECEIVED,
        self::STATUS_DELIVERED,
    ];
}
