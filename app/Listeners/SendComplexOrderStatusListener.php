<?php

namespace App\Listeners;

use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class SendComplexOrderStatusListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(private Client $httpClient)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     * @throws GuzzleException
     */
    public function handle($event)
    {

        $orderId   = $event->entity->id;
        $status    = $event->entity->status;
        $complexId = $event->entity->complex_id;

        $message    = 'the order number' . $orderId . 'updated status to' . $status;
        $webHookUrl = User::find($complexId)->web_hook_url;
        $header     = ['Content-Type' => 'application/json'];
        $data       = [
            'body'    => $message,
            'headers' => $header,
        ];

        //ToDo request to webHook url, new status of order to complex
//        $this->httpClient->post($webHookUrl, $data);
    }
}
