<?php

namespace App\Models;

use App\Casts\DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'complex_id',
        'provider_name',
        'provider_mobile',
        'provider_address',
        'provider_latitude',
        'provider_longitude',
        'receiver_name',
        'receiver_mobile',
        'receiver_address',
        'receiver_latitude',
        'receiver_longitude',
        'status',
        'courier_id',
    ];

    protected $casts = [
        'created_at' => DateTime::class,
        'updated_at' => DateTime::class,
    ];

    public function complex()
    {
        return $this->belongsTo(User::class, 'complex_id');
    }
}
