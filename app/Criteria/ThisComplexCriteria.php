<?php

namespace App\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class ThisComplexCriteria.
 *
 * @author  Mahmoud Zalt <mahmoud@zalt.me>
 */
class ThisComplexCriteria implements CriteriaInterface
{
    /**
     * @var int
     */
    private int $complexId;

    /**
     * ThisComplexCriteria constructor.
     *
     * @param $complexId
     */
    public function __construct($complexId = null)
    {
        $this->complexId = $complexId;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): mixed
    {
        if (!$this->complexId) {
            $this->complexId = Auth::user()->id;
        }

        return $model->where('complex_id', '=', $this->complexId);
    }
}
