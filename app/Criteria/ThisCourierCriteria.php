<?php

namespace App\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class ThisCourierCriteria.
 *
 * @author  Mahmoud Zalt <mahmoud@zalt.me>
 */
class ThisCourierCriteria implements CriteriaInterface
{
    /**
     * @var int
     */
    private int $courierId;

    /**
     * ThisComplexCriteria constructor.
     *
     * @param $courierId
     */
    public function __construct($courierId = null)
    {
        $this->courierId = $courierId;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): mixed
    {
        if (!$this->courierId) {
            $this->courierId = Auth::user()->id;
        }

        return $model->where('courier_id', '=', $this->courierId);
    }
}
