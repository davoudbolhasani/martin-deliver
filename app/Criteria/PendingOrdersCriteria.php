<?php

namespace App\Criteria;

use App\Enum\OrderEnum;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class PendingOrdersCriteria.
 *
 * @author  Mahmoud Zalt <mahmoud@zalt.me>
 */
class PendingOrdersCriteria implements CriteriaInterface
{
    /**
     * @param $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): mixed
    {
        return $model->where('status', '=', OrderEnum::STATUS_PENDING);
    }
}
