<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'complex_name'       => $this->complex->name,
            'provider_name'      => $this->provider_name,
            'provider_mobile'    => $this->provider_mobile,
            'provider_address'   => $this->provider_address,
            'provider_latitude'  => $this->provider_latitude,
            'provider_longitude' => $this->provider_longitude,
            'receiver_name'      => $this->receiver_name,
            'receiver_mobile'    => $this->receiver_mobile,
            'receiver_address'   => $this->receiver_address,
            'receiver_latitude'  => $this->receiver_latitude,
            'receiver_longitude' => $this->receiver_longitude,
            'status'             => $this->status,
            'created_at'         => $this->created_at,
            'updated_at'         => $this->updated_at,
        ];
    }
}
