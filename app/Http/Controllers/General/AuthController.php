<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Services\Authentication\AuthenticationServiceInterface;

class AuthController extends Controller
{
    /**
     * @param AuthenticationServiceInterface $service
     */
    public function __construct(protected AuthenticationServiceInterface $service)
    {

    }

    /**
     * @param LoginRequest $request
     * @return void
     */
    public function loginUsingPasswordGrant(LoginRequest $request)
    {
        $result = $this->service->login($request);

        return $result->json();
    }
}
