<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Complex\ComplexCancelOrderRequest;
use App\Http\Requests\Complex\CreateOrderRequest;
use App\Http\Requests\Complex\GetAllComplexOrdersRequest;
use App\Http\Requests\Courier\CourierUpdateOrderStatusRequest;
use App\Http\Requests\Courier\GetAllCourierOrdersRequest;
use App\Http\Requests\Courier\GetAllPendingOrdersRequest;
use App\Http\Resources\OrderResource;
use App\Services\Api\V1\Order\OrderServiceInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OrderController extends Controller
{
    /**
     * @param OrderServiceInterface $service
     */
    public function __construct(protected OrderServiceInterface $service)
    {

    }

    /**
     * @param GetAllComplexOrdersRequest $request
     * @return AnonymousResourceCollection
     */
    public function index(GetAllComplexOrdersRequest $request): AnonymousResourceCollection
    {
        $orders = $this->service->getAllComplexOrders($request);

        return OrderResource::collection($orders);
    }

    /**
     * @param CreateOrderRequest $request
     * @return OrderResource
     */
    public function store(CreateOrderRequest $request): OrderResource
    {
        $order = $this->service->store($request);

        return new OrderResource($order);
    }

    /**
     * @param ComplexCancelOrderRequest $request
     */
    public function destroy(ComplexCancelOrderRequest $request)
    {
        return $this->service->complexCancelOrder($request);
    }

    /**
     * @param GetAllPendingOrdersRequest $request
     * @return AnonymousResourceCollection
     */
    public function getAllPendingOrders(GetAllPendingOrdersRequest $request): AnonymousResourceCollection
    {
        $orders = $this->service->getAllPendingOrders($request);

        return OrderResource::collection($orders);
    }

    /**
     * @param GetAllCourierOrdersRequest $request
     * @return AnonymousResourceCollection
     */
    public function getAllCourierOrders(GetAllCourierOrdersRequest $request): AnonymousResourceCollection
    {
        $orders = $this->service->getAllCourierOrders($request);

        return OrderResource::collection($orders);
    }


    /**
     * @param CourierUpdateOrderStatusRequest $request
     * @return OrderResource
     */
    public function courierUpdateStatus(CourierUpdateOrderStatusRequest $request): OrderResource
    {
        $order = $this->service->courierUpdateStatus($request);

        return new OrderResource($order);
    }
}
