<?php

namespace App\Http\Requests\Complex;

use App\Http\Requests\Abstracts\ComplexRequest;

class GetAllComplexOrdersRequest extends ComplexRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }

}
