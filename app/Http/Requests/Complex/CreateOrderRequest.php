<?php

namespace App\Http\Requests\Complex;

use App\Http\Requests\Abstracts\ComplexRequest;

class CreateOrderRequest extends ComplexRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_name'      => 'required|string',
            'provider_mobile'    => 'required|regex:/(09)[0-9]{9}/|digits:11',
            'provider_address'   => 'required|string',
            'provider_latitude'  => 'required|between:0,99.9999999',
            'provider_longitude' => 'required|between:0,99.9999999',
            'receiver_name'      => 'required|string',
            'receiver_mobile'    => 'required|regex:/(09)[0-9]{9}/|digits:11',
            'receiver_address'   => 'required|string',
            'receiver_latitude'  => 'required|between:0,99.9999999',
            'receiver_longitude' => 'required|between:0,99.9999999',
        ];
    }

}
