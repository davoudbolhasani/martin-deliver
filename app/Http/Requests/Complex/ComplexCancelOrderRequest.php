<?php

namespace App\Http\Requests\Complex;

use App\Http\Requests\Abstracts\ComplexRequest;
use App\Http\Requests\Traits\IsAllowComplexCancelOrder;
use App\Http\Requests\Traits\IsComplexOwnerOrder;
use App\Services\Api\V1\Order\OrderAuthorizationServiceInterface;

class ComplexCancelOrderRequest extends ComplexRequest
{
    use IsComplexOwnerOrder;
    use IsAllowComplexCancelOrder;

    /**
     * @param OrderAuthorizationServiceInterface $service
     */
    public function __construct(protected OrderAuthorizationServiceInterface $service)
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge(['id' => $this->route('complex_order')]);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return  bool
     */
    public function authorize(): bool
    {
        return $this->check([
            'hasAccess',
            'isComplexOwnerOrder',
            'isAllowComplexCancelOrder',
        ]);
    }

}
