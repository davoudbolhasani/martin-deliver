<?php

namespace App\Http\Requests\Courier;

use App\Http\Requests\Abstracts\CourierRequest;

class GetAllCourierOrdersRequest extends CourierRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

}
