<?php

namespace App\Http\Requests\Courier;

use App\Enum\OrderEnum;
use App\Http\Requests\Abstracts\CourierRequest;
use App\Http\Requests\Traits\IsAllowCourierUpdateStatusOrder;
use App\Http\Requests\Traits\IsCourierOwnerOrder;
use App\Services\Api\V1\Order\OrderAuthorizationServiceInterface;

class CourierUpdateOrderStatusRequest extends CourierRequest
{
    use IsCourierOwnerOrder;
    use IsAllowCourierUpdateStatusOrder;

    /**
     * @param OrderAuthorizationServiceInterface $service
     */
    public function __construct(protected OrderAuthorizationServiceInterface $service)
    {
        parent::__construct();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|in:' . implode(',', OrderEnum::$courierOrderStatus),
        ];
    }

    /**
     * @return  bool
     */
    public function authorize(): bool
    {
        return $this->check([
            'hasAccess',
            'isCourierOwnerOrder',
            'isAllowCourierUpdateStatusOrder',
        ]);
    }
}
