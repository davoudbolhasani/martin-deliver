<?php

namespace App\Http\Requests\Abstracts;

class CourierRequest extends BaseRequest
{
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => 'courier',
    ];
}
