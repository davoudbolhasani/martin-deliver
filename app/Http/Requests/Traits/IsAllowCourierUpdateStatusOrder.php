<?php

namespace App\Http\Requests\Traits;


trait IsAllowCourierUpdateStatusOrder
{
    public function isAllowCourierUpdateStatusOrder()
    {
        $orderId = $this->id;
        $status  = $this->status;

        return $this->service->isAllowCourierUpdateStatusOrder($orderId, $status);
    }
}
