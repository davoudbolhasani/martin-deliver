<?php

namespace App\Http\Requests\Traits;

trait IsCourierOwnerOrder
{
    public function isCourierOwnerOrder()
    {
        $userId  = $this->user()->id;
        $orderId = $this->id;

        return $this->service->isCourierOwnerOrder($userId, $orderId);
    }
}
