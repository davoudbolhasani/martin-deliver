<?php

namespace App\Http\Requests\Traits;


trait IsAllowComplexCancelOrder
{
    public function isAllowComplexCancelOrder()
    {
        $orderId = $this->id;

        return $this->service->isAllowComplexCancelOrder($orderId);
    }
}
