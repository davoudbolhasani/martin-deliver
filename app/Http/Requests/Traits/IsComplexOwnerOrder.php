<?php

namespace App\Http\Requests\Traits;

trait IsComplexOwnerOrder
{
    public function isComplexOwnerOrder()
    {
        $userId = $this->user()->id;
        $orderId = $this->id;

        return $this->service->isComplexOwnerOrder($userId, $orderId);
    }
}
