<?php

namespace App\Services\Api\V1\Order;

use App\Enum\OrderEnum;
use App\Repositories\Order\OrderRepositoryInterface;
use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Exceptions\RepositoryException;

class OrderAuthorizationService implements OrderAuthorizationServiceInterface
{
    /**
     * @param OrderRepositoryInterface $repository
     */
    public function __construct(protected OrderRepositoryInterface $repository)
    {

    }

    /**
     * @param $userId
     * @param $orderId
     * @return bool
     */
    public function isComplexOwnerOrder($userId, $orderId): bool
    {
        $order = $this->repository->findwhere(['id' => $orderId, 'complex_id' => $userId])->first();

        if ($order) {
            return true;
        }

        return false;
    }

    /**
     * @param $orderId
     * @return bool
     */
    public function isAllowComplexCancelOrder($orderId): bool
    {
        $order = $this->repository->find($orderId);

        if (in_array($order->status, OrderEnum::$complexAllowCancelOrderStatus)) {
            return true;
        }

        return false;
    }

    /**
     * @param $userId
     * @param $orderId
     * @return bool
     * @throws RepositoryException
     */
    public function isCourierOwnerOrder($userId, $orderId): bool
    {
        $lockOrder = Cache::get('lock-order-' . $orderId);

        if (is_null($lockOrder) || $lockOrder == $userId) {
            $order = $this->repository->makeModel();
            $order = $order->where('id', $orderId)->where(function ($query) use ($userId) {
                $query->where('courier_id', $userId)
                    ->orWhereNull('courier_id');
            })->first();

            if ($order) {
                Cache::put('lock-order-' . $orderId, $userId, config('app.cache_time.one_minute'));
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * @param $orderId
     * @param $status
     * @return bool
     */
    public function isAllowCourierUpdateStatusOrder($orderId, $status): bool
    {
        $order = $this->repository->find($orderId);

        if ($order) {
            return match ($status) {
                OrderEnum::STATUS_ACCEPTED  => ($order->status == OrderEnum::STATUS_PENDING),
                OrderEnum::STATUS_RECEIVED  => ($order->status == OrderEnum::STATUS_ACCEPTED),
                OrderEnum::STATUS_DELIVERED => ($order->status == OrderEnum::STATUS_RECEIVED),
            };
        }

        return false;
    }
}
