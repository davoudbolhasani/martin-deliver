<?php

namespace App\Services\Api\V1\Order;

interface OrderServiceInterface
{
    public function getAllComplexOrders($request);

    public function store($request);

    public function complexCancelOrder($request);

    public function getAllPendingOrders($request);

    public function getAllCourierOrders($request);

    public function courierUpdateStatus($request);
}
