<?php

namespace App\Services\Api\V1\Order;

use App\Criteria\PendingOrdersCriteria;
use App\Criteria\ThisComplexCriteria;
use App\Criteria\ThisCourierCriteria;
use App\Enum\OrderEnum;
use App\Events\SendComplexOrderStatusEvent;
use App\Exceptions\Complex\ComplexCancelOrderException;
use App\Exceptions\Complex\ComplexCreateOrderException;
use App\Exceptions\Complex\GetAllComplexOrdersException;
use App\Exceptions\Courier\GetAllCourierOrdersException;
use App\Repositories\Order\OrderRepositoryInterface;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class OrderService implements OrderServiceInterface
{
    /**
     * @param OrderRepositoryInterface $repository
     */
    public function __construct(protected OrderRepositoryInterface $repository)
    {

    }

    /**
     * @param $request
     * @return void
     * @throws GetAllComplexOrdersException
     */
    public function getAllComplexOrders($request)
    {
        try {
            $complexId = $request->user()->id;
            return $this->repository
                ->pushCriteria(new ThisComplexCriteria($complexId))
                ->orderBy('created_at', 'desc')
                ->paginate();
        } catch (Exception $error) {
            throw new GetAllComplexOrdersException();
        }
    }

    /**
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function store($request): mixed
    {
        try {
            $userId             = $request->user()->id;
            $data               = $request->validated();
            $data['complex_id'] = $userId;
            $data['status']     = OrderEnum::STATUS_PENDING;

            return $this->repository->create($data);
        } catch (Exception $error) {
            throw new ComplexCreateOrderException();
        }
    }

    /**
     * @param $request
     * @return LengthAwarePaginator|Collection|mixed|void
     * @throws Exception
     */
    public function complexCancelOrder($request)
    {
        try {
            $data = [
                'status' => OrderEnum::STATUS_CANCELED,
            ];

            $this->repository->update($data, $request->id);
            $result['data'] = [
                'message' => 'your order canceled successfully',
            ];
            return $result;
        } catch (Exception $error) {
            throw new ComplexCancelOrderException();
        }
    }

    /**
     * @throws Exception
     */
    public function getAllPendingOrders($request)
    {
        try {
            return $this->repository
                ->pushCriteria(new PendingOrdersCriteria())
                ->orderBy('created_at', 'desc')
                ->paginate();
        } catch (Exception $error) {
            throw new Exception();
        }
    }

    /**
     * @param $request
     * @return void
     * @throws GetAllCourierOrdersException
     */
    public function getAllCourierOrders($request)
    {
        try {
            $complexId = $request->user()->id;
            return $this->repository
                ->pushCriteria(new ThisCourierCriteria($complexId))
                ->orderBy('created_at', 'desc')
                ->paginate();
        } catch (Exception $error) {
            throw new GetAllCourierOrdersException();
        }
    }

    /**
     * @param $request
     * @return LengthAwarePaginator|Collection|mixed|void
     * @throws Exception
     */
    public function courierUpdateStatus($request)
    {
        try {
            $userId = $request->user()->id;
            $data   = [
                'status'     => $request->status,
                'courier_id' => $userId,
            ];
            $order = $this->repository->update($data, $request->id);

            event(new SendComplexOrderStatusEvent($order));
            return $order;
        } catch (Exception $error) {
            throw new Exception();
        }
    }
}
