<?php

namespace App\Services\Api\V1\Order;

interface OrderAuthorizationServiceInterface
{
    public function isComplexOwnerOrder($userId, $orderId);

    public function isAllowComplexCancelOrder($orderId);

    public function isCourierOwnerOrder($userId, $orderId);

    public function isAllowCourierUpdateStatusOrder($orderId, $status);
}
