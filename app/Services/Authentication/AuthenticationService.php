<?php

namespace App\Services\Authentication;

use App\Exceptions\Authentication\LoginFailedException;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class AuthenticationService implements AuthenticationServiceInterface
{
    /**
     * @string
     */
    const AUTH_ROUTE = '/oauth/token';

    /**
     * @param $request
     * @return PromiseInterface|Response
     */
    public function login($request): PromiseInterface|Response
    {
        $data = [
            'username' => $request->email,
            'password' => $request->password,
        ];

        $loginData = array_merge($data, [
            'grant_type'    => $request->grant_type ?? 'password',
            'client_id'     => Config::get('auth.clients.web.admin.id'),
            'client_secret' => Config::get('auth.clients.web.admin.secret'),
            'scope'         => '',
        ]);

        return $this->callOAuth($loginData);
    }

    public function callOAuth($data): PromiseInterface|Response
    {
        $authFullApiUrl = Config::get('app.url') . self::AUTH_ROUTE;
        $response = Http::asForm()->post($authFullApiUrl, $data);

        if (!$response->successful()) {
            throw new LoginFailedException(statusCode : $response->status());
        }

        return $response;
    }
}
