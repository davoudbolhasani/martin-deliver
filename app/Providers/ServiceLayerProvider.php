<?php

namespace App\Providers;

use App\Services\Api\V1\Order\OrderAuthorizationService;
use App\Services\Api\V1\Order\OrderAuthorizationServiceInterface;
use App\Services\Api\V1\Order\OrderService;
use App\Services\Api\V1\Order\OrderServiceInterface;
use App\Services\Authentication\AuthenticationService;
use App\Services\Authentication\AuthenticationServiceInterface;
use Illuminate\Support\ServiceProvider;

class ServiceLayerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthenticationServiceInterface::class, AuthenticationService::class);
        $this->app->bind(OrderServiceInterface::class, OrderService::class);
        $this->app->bind(OrderAuthorizationServiceInterface::class, OrderAuthorizationService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
