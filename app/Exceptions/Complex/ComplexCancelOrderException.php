<?php

namespace App\Exceptions\Complex;

use App\Exceptions\Abstracts\CustomErrorCodesTable;
use App\Exceptions\Abstracts\ParentException;
use Symfony\Component\HttpFoundation\Response;

class ComplexCancelOrderException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::COMPLEX_CANCEL_ORDER_FAILED['title'];

    public $code = CustomErrorCodesTable::COMPLEX_CANCEL_ORDER_FAILED['code'];
}
