<?php

namespace App\Exceptions\Complex;

use App\Exceptions\Abstracts\CustomErrorCodesTable;
use App\Exceptions\Abstracts\ParentException;
use Symfony\Component\HttpFoundation\Response;

class GetAllComplexOrdersException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::GET_COMPLEX_ORDERS_FAILED['title'];

    public $code = CustomErrorCodesTable::GET_COMPLEX_ORDERS_FAILED['code'];
}
