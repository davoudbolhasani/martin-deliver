<?php

namespace App\Exceptions\Complex;

use App\Exceptions\Abstracts\CustomErrorCodesTable;
use App\Exceptions\Abstracts\ParentException;
use Symfony\Component\HttpFoundation\Response;

class ComplexUpdateOrderException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::COMPLEX_UPDATE_ORDER_FAILED['title'];

    public $code = CustomErrorCodesTable::COMPLEX_UPDATE_ORDER_FAILED['code'];
}
