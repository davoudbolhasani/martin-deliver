<?php

namespace App\Exceptions\Authorization;

use App\Exceptions\Abstracts\CustomErrorCodesTable;
use App\Exceptions\Abstracts\ParentException;
use Symfony\Component\HttpFoundation\Response;

class AuthorizationException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::ACCESS_DENIED['title'];

    public $code = CustomErrorCodesTable::ACCESS_DENIED['code'];
}
