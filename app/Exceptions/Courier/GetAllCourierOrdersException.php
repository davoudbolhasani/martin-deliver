<?php

namespace App\Exceptions\Courier;

use App\Exceptions\Abstracts\CustomErrorCodesTable;
use App\Exceptions\Abstracts\ParentException;
use Symfony\Component\HttpFoundation\Response;

class GetAllCourierOrdersException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::GET_COURIER_ORDERS_FAILED['title'];

    public $code = CustomErrorCodesTable::GET_COURIER_ORDERS_FAILED['code'];
}
