<?php

namespace App\Exceptions\Abstracts;


/**
 * Class CustomErrorCodesTable
 *
 * @author  Johannes Schobel <johannes.schobel@googlemail.com>
 */
class CustomErrorCodesTable
{
    /**
     * Use this class to define your own custom error code tables. Please follow the scheme defined in the other file
     * in order to make them compliant!
     *
     * Please note that Apiato reserves the error codes 000000 - 099999 for itself. If you define your own codes,
     * please start with 100000
     *
     * const BASE_GENERAL_ERROR = [
     *      'code' => 100000,
     *      'title' => 'Unknown / Unspecified Error.',
     *      'description' => 'Something unexpected happened.',
     *  ];
     *
     */
    const LOGIN_FAILED = [
        'code'        => 3000,
        'title'       => 'اطلاعات ورود صحیح نمی باشد',
        'description' => 'An Exception happened during the Login Process.',
    ];
    const ACCESS_DENIED = [
        'code'        => 3001,
        'title'       => 'شما دسترسی لازم برای این درخواست را ندارید',
        'description' => 'No Access',
    ];

    const GET_COMPLEX_ORDERS_FAILED = [
        'code'        => 3002,
        'title'       => 'دریافت لیست سفارشات شما با خطا مواجه شد',
        'description' => 'get your list of orders failed!',
    ];

    const COMPLEX_CREATE_ORDER_FAILED = [
        'code'        => 3003,
        'title'       => 'ثبت سفارش با خطا مواجه شد!',
        'description' => 'create order failed!',
    ];

    const COMPLEX_UPDATE_ORDER_FAILED = [
        'code'        => 3004,
        'title'       => 'شما اجازه به روزرسانی سفارش را ندارید',
        'description' => 'update order failed!',
    ];

    const COMPLEX_CANCEL_ORDER_FAILED = [
        'code'        => 3005,
        'title'       => 'شما اجازه لغو این سفارش را ندارید',
        'description' => 'update order failed!',
    ];

    const GET_COURIER_ORDERS_FAILED = [
        'code'        => 3006,
        'title'       => 'دریافت لیست سفارشات شما با خطا مواجه شد',
        'description' => 'get your list of orders failed!',
    ];

    const GET_PENDING_ORDERS_FAILED = [
        'code'        => 3007,
        'title'       => 'دریافت لیست سفارشات با خطا مواجه شد',
        'description' => 'get list of orders failed!',
    ];
}
