<?php

use App\Http\Controllers\Api\V1\OrderController;
use Illuminate\Support\Facades\Route;

#Complex APIs
Route::apiResource('complex-orders', 'OrderController')->except('update');


#Courier APIs
Route::prefix('courier-orders')->group(function () {
    Route::get('', [OrderController::class, 'getAllCourierOrders']);
    Route::get('/pending', [OrderController::class, 'getAllPendingOrders']);
    Route::patch('/{id}/status', [OrderController::class, 'courierUpdateStatus']);
});
