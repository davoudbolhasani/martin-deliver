########################## Martin Deliver ###############################

#Run project
- you can run the project by following the steps below:
 
  - clone the project
  - copy .env.example to .env
  - composer install
  - set db environment
  - crate database
  - run migration
  - run seeder
  - run php artisan passport:keys for generate oauth keys
  - login with test users:
      - first-complex : first_complex@example.com
      - second-complex : second_complex@example.com
      - password = 123456
    
      - first-courier : first_courier@example.com
      - second-courier : second_courierx@example.com
      - password = 123456



########################## martin deliver ###############################



#Complex APIs (prefix: /api/v1)
- login:
  - POST /login

- complex order list:
  - GET /complex-orders
  
- create order:
  - POST /complex-orders
  - example request body :{
    "provider_name":"david",
    "provider_mobile":"09189612539",
    "provider_address":"gharb",
    "provider_latitude":"20.845642",
    "provider_longitude":"21.365214",
    "receiver_name":"javad",
    "receiver_mobile":"09359341940",
    "receiver_address":"shargh",
    "receiver_latitude":"10.326547",
    "receiver_longitude":"11.324658",
    }

- cancel order:
  - DELETE /complex-orders/{id}


#Courier APIs (prefix: /api/v1)
- login:
    - POST /login

- list of courier own orders
    - GET /courier-orders

- list of all pending orders
    - GET /courier-orders/pending

- update status of order(accepted, received, delivered):
    - PATCH /courier-orders/{id}/status
    - example request body: {"status":"accepted" or "received" or "delivered"}
