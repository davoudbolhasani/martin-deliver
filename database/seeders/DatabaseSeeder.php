<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OauthClientSeeder::class);
        $this->call(RoleSeeder::class);

        $first_complexUser  = \App\Models\User::factory()->create(['email' => 'first_complex@example.com']);
        $second_complexUser = \App\Models\User::factory()->create(['email' => 'second_complex@example.com']);
        $first_complexUser->assignRole('complex');
        $second_complexUser->assignRole('complex');

        $first_courierUser  = \App\Models\User::factory()->create(['email' => 'first_courier@example.com']);
        $second_courierUser = \App\Models\User::factory()->create(['email' => 'second_courier@example.com']);
        $first_courierUser->assignRole('courier');
        $second_courierUser->assignRole('courier');
    }
}
